section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi + rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10
    mov r10, rsp
	dec rsp
	xor rdx, rdx
	.loop:
		div rcx
		add dl, '0'
		dec rsp
		mov byte [rsp], dl
		xor rdx, rdx
		cmp rax, 0
		jnz .loop
	mov rdi, rsp
	push r10
	call print_string
	pop rsp
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    	cmp rdi, 0
    	jge .positive_number
    	neg rdi
	push rdi
    	mov rdi, '-'
    	call print_char
    	pop rdi
	.positive_number:
		jmp print_uint
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r10b, byte [rdi]
		cmp r10b, byte [rsi]
        jne .not_equals
		
		inc rdi
        inc rsi 
        
		cmp r10b, 0
        je .end
        
		jmp .loop
    .end:
        mov rax, 1
        ret
    .not_equals:
        mov rax, 0
        ret
	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    	xor rax, rax
    	mov rax, 0
    	mov rdi, 0
    	push 0
    	mov rsi, rsp
    	mov rdx, 1
    	syscall
		test rax, rax
		jz .end
		js .read_error
		pop rax
		ret
	.end:
    	pop rax
    	ret
	.read_error:
		pop rax
    	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rax, rax
    push r12
    push r13
    push r14
    mov r12, rsi
    mov r13, rdi
    
	.check:
        xor r14, r14
        call read_char

        cmp rax, 0x20
        jz .check

        cmp rax, 0x9
        jz .check

        cmp rax, 0xA
        jz .check

        cmp rax, 0
        jz .null_in_rax
        jmp .write
    
	.read:
        call read_char
        cmp rax, 0
        jz .save_end
		cmp rax, 0x20
        jz .save_end
		cmp rax, 0x9
        jz .save_end
		cmp rax, 0xA
        jz .save_end
    
	.write:
        mov byte[r13 + r14], al
        inc r14
        cmp r14, r12 
        jz .null_in_rax
        jmp .read
    
	.save_end:
        mov byte[r13 + r14], 0
        mov rax, r13
        mov rdx, r14
        jmp .end
    
	.null_in_rax:
        xor rax, rax
        xor rdx, rdx
        jmp .end
    
	.end:
        pop r14
        pop r13
        pop r12
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor	rdx, rdx
	xor	rax, rax
	xor	rcx, rcx
	.loop:
		mov	al, [rdi + rcx]
		test al, al
		jz	.end
		sub	al, '0'		
		js	.end
		cmp	al, 9
		ja	.end
		inc	rcx
		imul rdx, 10
		add	dl, al
		jmp	.loop
	.end:
		mov	rax, rdx
		mov	rdx, rcx
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov	al, [rdi]
	cmp	al, '-'
	je 	.negative_num
	.positive_num:
		call parse_uint
		ret
	.negative_num:
		inc	rdi
		call parse_uint
		neg	rax
		inc	rdx
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	xor r9, r9
    .loop:
    	cmp rax, rdx
		jge .not
		mov r9b, byte [rdi + rax]
		mov byte [rsi + rax], r9b
		test r9b, r9b
		jz .exit
		inc rax
		jmp .loop
	.not:
		xor rax, rax
	.exit:
		ret	
